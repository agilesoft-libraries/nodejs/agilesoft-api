export interface IHeaders {
    action: string;
    context: string;
    trxid: string;
    executiveid: string;
    secondaryuserid: string;
    userid: string;
    sessionid: string;
    module: string;
    service: string;
}
