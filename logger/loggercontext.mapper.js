"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.headerContextDataMapper = exports.sessionContextDataMapper = void 0;
const sessionContextDataMapper = (request) => {
    const session = request.storages['usuario'];
    const response = {
        transaction: {
            action: '',
            context: session.data.context,
            module: '',
            service: '',
            timestamp: new Date(),
            trxId: session.sessionID,
        },
        client: {
            executiveID: '',
            secondaryUserID: String(session.data.rut),
            userID: String(session.data.rut),
            sessionID: session.sessionID,
        }
    };
    return Promise.resolve(response);
};
exports.sessionContextDataMapper = sessionContextDataMapper;
const headerContextDataMapper = (request) => {
    const headers = request.headers;
    const response = {
        transaction: {
            action: headers.action,
            context: headers.context,
            module: headers.module,
            service: headers.service,
            timestamp: new Date(),
            trxId: headers.trxid,
        },
        client: {
            executiveID: headers.executiveid,
            secondaryUserID: headers.secondaryuserid,
            userID: headers.userid,
            sessionID: headers.sessionid,
        }
    };
    return Promise.resolve(response);
};
exports.headerContextDataMapper = headerContextDataMapper;
//# sourceMappingURL=loggercontext.mapper.js.map