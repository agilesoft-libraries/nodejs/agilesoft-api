import { ApiLoggerContextData } from "agilesoft-logger/context/loggerdata.interface";
export declare const sessionContextDataMapper: (request: any) => Promise<ApiLoggerContextData>;
export declare const headerContextDataMapper: (request: any) => Promise<ApiLoggerContextData>;
