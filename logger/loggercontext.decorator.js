"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerContext = void 0;
const common_1 = require("@nestjs/common");
exports.LoggerContext = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return request.loggerInstance;
});
//# sourceMappingURL=loggercontext.decorator.js.map