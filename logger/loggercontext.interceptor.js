"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggingContextInterceptor = void 0;
const common_1 = require("@nestjs/common");
const operators_1 = require("rxjs/operators");
const context_logger_1 = require("agilesoft-logger/context/context.logger");
let LoggingContextInterceptor = class LoggingContextInterceptor {
    intercept(context, next) {
        const request = context.switchToHttp().getRequest();
        const loggerContextData = request.loggerContextData;
        loggerContextData.transaction.action = context.getHandler().name;
        loggerContextData.transaction.service = context.getClass().name;
        loggerContextData.transaction.timestamp = new Date();
        request.loggerInstance = new context_logger_1.ContextLogger({
            data: request.loggerContextData,
        });
        return next.handle().pipe(operators_1.tap(() => {
            return;
        }));
    }
};
LoggingContextInterceptor = __decorate([
    common_1.Injectable()
], LoggingContextInterceptor);
exports.LoggingContextInterceptor = LoggingContextInterceptor;
//# sourceMappingURL=loggercontext.interceptor.js.map