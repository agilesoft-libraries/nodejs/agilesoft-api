import { RequestHandler } from 'express';
import { StorageConfiguration } from "agilesoft-framework/storage/storage.interfaces";
import { SessionMiddlewareConfig, HeadersMiddlewareConfig } from './session.types';
export declare const initialization: (config: StorageConfiguration) => void;
export declare const createSessionMiddleware: (config: SessionMiddlewareConfig) => RequestHandler;
export declare const createHeadersMiddleware: (config: HeadersMiddlewareConfig) => RequestHandler;
