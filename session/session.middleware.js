"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createHeadersMiddleware = exports.createSessionMiddleware = exports.initialization = void 0;
const frameworkStorage = __importStar(require("agilesoft-framework/storage/storage.functions"));
const jwtUtil = __importStar(require("../jwt/jwt.functions"));
const loggerUtil = __importStar(require("agilesoft-logger/logger"));
const logger = loggerUtil.createInstance('agilesoft-api.session.middleware');
const launchError = (message, response) => {
    logger.error(message);
    response.status(401);
    return response.send(`Request no posee información de sesion valida`);
};
const initialization = (config) => {
    frameworkStorage.configuration(config);
};
exports.initialization = initialization;
const createSessionMiddleware = (config) => {
    return (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
        request.storages = [];
        if (String(request.excludeSession) === 'true') {
            return next();
        }
        const jwtToken = jwtUtil.getStringToken(request);
        if (!jwtToken) {
            return launchError('Token JWT no viene en headers', response);
        }
        const decodeJwt = jwtUtil.decodeToken(request);
        if (!decodeJwt) {
            return launchError('Token JWT no valido', response);
        }
        if (String(process.env.TEST_MODE) === 'true') {
            for (const storage of config.storages) {
                request.storages[storage.storageKey] = decodeJwt;
            }
        }
        else {
            const promises = [];
            const isMandatoryMap = new Map();
            for (const storage of config.storages) {
                isMandatoryMap.set(storage.storageKey, storage.mandatory);
                promises.push(frameworkStorage.getStorage({ key: storage.storageKey, jwtSession: jwtToken }));
            }
            const responses = yield Promise.all(promises);
            for (const storageResponse of responses) {
                if (storageResponse.status !== 200 && isMandatoryMap.get(storageResponse.trxId)) {
                    return launchError(`No se encuentra información para la llave ${storageResponse.trxId}`, response);
                }
                request.storages[storageResponse.trxId] = storageResponse ? {
                    sessionID: decodeJwt.sessionID,
                    iat: decodeJwt.iat,
                    data: storageResponse.data.data,
                } : null;
            }
        }
        if (config.contextDataMapperFunction) {
            try {
                const contextData = yield config.contextDataMapperFunction(request);
                if (contextData) {
                    contextData.transaction.module = config.moduleName;
                    request.loggerContextData = contextData;
                }
            }
            catch (err) {
                return launchError('Error al procesar request para generar datos de contexto de logger', err);
            }
        }
        return next();
    });
};
exports.createSessionMiddleware = createSessionMiddleware;
const createHeadersMiddleware = (config) => {
    return (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
        if (String(request.excludeSession) === 'true') {
            return next();
        }
        if (config.contextDataMapperFunction) {
            try {
                const contextData = yield config.contextDataMapperFunction(request);
                if (contextData) {
                    contextData.transaction.module = config.moduleName;
                    request.loggerContextData = contextData;
                }
            }
            catch (err) {
                return launchError('Error al procesar request para generar datos de contexto de logger', err);
            }
        }
        return next();
    });
};
exports.createHeadersMiddleware = createHeadersMiddleware;
//# sourceMappingURL=session.middleware.js.map