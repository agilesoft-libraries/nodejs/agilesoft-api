"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Session = void 0;
const common_1 = require("@nestjs/common");
exports.Session = common_1.createParamDecorator((storageKey, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    if (request.storages) {
        return request.storages[storageKey];
    }
    return null;
});
//# sourceMappingURL=session.decorator.js.map