import { ApiLoggerContextData } from "agilesoft-logger/context/loggerdata.interface";
export interface StorageDef {
    storageKey: string;
    mandatory: boolean;
}
export interface SessionMiddlewareConfig {
    storages: StorageDef[];
    moduleName: string;
    contextDataMapperFunction?: (request: any) => Promise<ApiLoggerContextData>;
}
export interface HeadersMiddlewareConfig {
    moduleName: string;
    contextDataMapperFunction?: (request: any) => Promise<ApiLoggerContextData>;
}
export interface SessionData<T> {
    sessionID: string;
    iat: number;
    data: T;
}
export interface Cliente {
    rut: number;
    dv: string;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    genero: string;
    nombreCompleto: string;
    tipoPersona: string;
    emailComercial: string;
    emailParticular: string;
    fechaNacimiento: string;
    telefono: string;
    username: string;
    context: string;
    roles: string[];
}
