export interface JwtConfiguration {
    headerTokenName?: string;
    hasExpiration?: boolean;
    experationSintax?: string;
    secret: string;
}
