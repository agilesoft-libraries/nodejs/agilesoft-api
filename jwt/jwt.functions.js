"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodeToken = exports.generateToken = exports.checkFromHeader = exports.checkToken = exports.getStringToken = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const loggerUtil = __importStar(require("agilesoft-logger/logger"));
const logger = loggerUtil.createInstance("agilesfot-api.jwt");
const TOKEN = "x-access-token";
const TOKENA = "authorization";
const TOKENB = "Authorization";
const getStringToken = (req, headerName) => {
    let token;
    if (headerName) {
        token = req.headers[headerName];
    }
    else {
        token = req.headers[TOKEN] || req.headers[TOKENA] || req.headers[TOKENB];
    }
    if (token && token.startsWith("Bearer ")) {
        token = token.slice(7, token.length);
    }
    return token;
};
exports.getStringToken = getStringToken;
const checkToken = (token, jwtConfig) => {
    try {
        let decodedData;
        if (String(process.env.TEST_MODE) === "true") {
            decodedData = jwt.decode(token);
        }
        else {
            decodedData = jwt.verify(token, jwtConfig.secret);
        }
        return decodedData;
    }
    catch (err) {
        // el token no fue generado desde una fuente confiable
        logger.error(`Token de sesion no generado desde una fuente confiable ${token}`, err);
        return null;
    }
};
exports.checkToken = checkToken;
const checkFromHeader = (req, jwtConfig) => {
    // se obtiene el valor del token desde los header de una peticion HTTP
    const token = (jwtConfig.headerTokenName && req.headers[jwtConfig.headerTokenName]) ||
        exports.getStringToken(req);
    // se verifica que el token haya sido generado desde una fuente confiable
    if (token) {
        return exports.checkToken(token, jwtConfig);
    }
    else {
        // no hay token en header, se rechaza la validacion
        logger.error(`Peticion HTTP sin token JWT ${req.url}`);
        return null;
    }
};
exports.checkFromHeader = checkFromHeader;
const generateToken = (data, jwtConfig) => {
    let response;
    if (String(jwtConfig.hasExpiration) === "true") {
        response = jwt.sign(data, jwtConfig.secret, {
            expiresIn: `${jwtConfig.experationSintax}`,
        });
    }
    else {
        response = jwt.sign(data, jwtConfig.secret);
    }
    return response;
};
exports.generateToken = generateToken;
const decodeToken = (req) => {
    const token = exports.getStringToken(req);
    if (token) {
        return jwt.decode(token);
    }
    return null;
};
exports.decodeToken = decodeToken;
//# sourceMappingURL=jwt.functions.js.map