import { JwtConfiguration } from "./jwt.interfaces";
export declare const getStringToken: (req: any, headerName?: string | undefined) => string;
export declare const checkToken: (token: string, jwtConfig: JwtConfiguration) => any;
export declare const checkFromHeader: (req: any, jwtConfig: JwtConfiguration) => any;
export declare const generateToken: (data: any, jwtConfig: JwtConfiguration) => string;
export declare const decodeToken: (req: any) => any;
